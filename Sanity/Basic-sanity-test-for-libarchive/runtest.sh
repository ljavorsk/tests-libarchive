#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/libarchive/Sanity/Basic-sanity-test-for-libarchive
#   Description: Encapsulates existing sanity tests built into the libarchive source package.
#   Author: Martin Cermak <mcermak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="libarchive"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        rm -rf ~/rpmbuild
        rlRun "pushd $TmpDir"
        packages="sharutils"
        if rlIsRHEL '>7'; then
            # TODO: remove once we have CRB available
            enable_repo="--enablerepo rhel-CRB"
            if grep -rq beaker-CRB /etc/yum.repos.d/; then
                enable_repo="--enablerepo beaker-CRB"
            fi
            rlRun "yum $enable_repo -y install $packages"
	elif rlIsCentOS; then
	    # Package shareutils is in CRB repo in CentOS Stream as well
	    rlRun "yum --enablerepo crb -y install $packages"
        fi
    rlPhaseEnd

    rlPhaseStartTest
        if rlIsRHEL; then
            rlFetchSrcForInstalled "libarchive" 0 "Download libarchive src rpm"
        else
            rlFetchSrcForInstalled "libarchive" 0 "Download libarchive src rpm"
            if [ ! -f ./libarchive-*.src.rpm ]; then
                rlRun "yumdownloader --source --enablerepo 'fedora-source' libarchive"
            fi
        fi

        rlRun "rpm -Uvh libarchive*.rpm" 0 "Install libarchive src rpm"

        rlRun "pushd ~/rpmbuild/SPECS" 0 "Go to the SPEC directory"
        if rlIsRHEL; then
            rlRun "yum-builddep libarchive -y" 0,1
        elif rlIsFedora; then
            rlRun "dnf builddep libarchive.spec"
        fi

        rlRun "rpmbuild -bp libarchive.spec"
        rlRun "rpmbuild -bc libarchive.spec"
        rlRun "popd"

        found=0
        for dir in ~/rpmbuild/BUILD/libarchive-*; do
            if [[ "$dir" == *"SPECPARTS"* ]]; then
                continue
            fi
            found=1
            rlLog "Found $dir"
            rlRun "pushd $dir"
            # This is the workaround to the RPM_ARCH not defined fail due to the package-notes dependency
            # More in this thread: https://lists.fedorahosted.org/archives/list/devel@lists.fedoraproject.org/thread/UHJGLOXDDDRTHG4SC5P5FDF37OAMNRDH/
            rlRun "eval \"$(rpm --eval %___build_pre)\"; cd $dir" 0 "setting environment"
            rlRun "make &> $TmpDir/make.log" 0-255 "Make."
            if [[ "$(rlGetArch)" == "s390x" ]]; then
                rlRun "make check &> $TmpDir/make_check.log || cat $TmpDir/make_check.log" 0,2 "Make check"
                rlRun -s "./libarchive_test" 0,1
                rlAssertGrep "Tests failed:[[:blank:]]* 1" "$rlRun_LOG"
                rlAssertGrep "test_write_filter_gzip (1 failures)" "$rlRun_LOG"
            else
                rlRun "make check &> $TmpDir/make_check.log || cat $TmpDir/make_check.log" 0 "Make check"
                rlRun -s "./libarchive_test"
                rlAssertGrep "Tests failed:[[:blank:]]* 0" "$rlRun_LOG"
            fi
            cat "$rlRun_LOG"
            cp "$rlRun_LOG" libarchive_test.log
            rlFileSubmit libarchive_test.log
            rlFileSubmit "$TmpDir/make_check.log"
        done
        if [[ "$found" -eq 0 ]]; then
            rlFail "Somethings went wrong when preparing BUILD files."
        fi

	if rlIsRHEL ">=7"; then
            rlRun "cat test-suite.log"
            rlFileSubmit "test-suite.log"
        fi
        rlRun "popd"

        rlRun "grep \"[0-9]:\ test_.*FAIL\" $TmpDir/make_check.log" 1 "Verify there was no error."
        rlRun "grep \"Segmentation fault\" $TmpDir/make_check.log" 1 "Verify there was no segfault."

    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs testlogs "$TmpDir/make.log" "$TmpDir/make_check.log"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
        rm -rf ~/rpmbuild
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
